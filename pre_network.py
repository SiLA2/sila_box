import yaml
from subprocess import check_output
import shlex

WLAN_DEV = "wlan0"
CONFIG_FILE = "/boot/SiLA/conf.yaml"

WPA_EXPORT_TARGET="/etc/wpa_supplicant/wpa_supplicant-wlan0.conf"
AP_EXPORT_TARGET="/etc/hostapd/hostapd.conf"


wpa_conf = """ctrl_interface=DIR=/var/run/wpa_supplicant GROUP=netdev
update_config=1

network={
     ssid="<ssid>"
     psk="<psk>"
}"""

hostapd_conf = """ctrl_interface=/var/run/hostapd
ctrl_interface_group=0
interface=wlan0
driver=nl80211
ssid=silabox_<id>
hw_mode=g
channel=11
wmm_enabled=0
macaddr_acl=0
auth_algs=1
wpa=2
wpa_passphrase=silamaster
wpa_key_mgmt=WPA-PSK
wpa_pairwise=TKIP CCMP
rsn_pairwise=CCMP"""

args = shlex.split("ifconfig %s" % WLAN_DEV)
out = check_output(args)
id = "None"
for s in out.decode().split("\n"):
    s = s.strip()
    if "ether" in s:
        id = s.split(" ")[1].replace(":", "")

hostapd_conf = hostapd_conf.replace("<id>", id)

with open(AP_EXPORT_TARGET, "w") as out:
        out.write(hostapd_conf)


with open(CONFIG_FILE, 'r') as stream:
    cfg_data = yaml.load(stream)

    wlan_conf = cfg_data.get("wlan", {})
    ssid = wlan_conf.get("ssid", "")
    psk = wlan_conf.get("psk", "")

    wpa_conf = wpa_conf.replace("<ssid>", ssid).replace("<psk>", psk)

    with open(WPA_EXPORT_TARGET, "w") as out:
        out.write(wpa_conf)