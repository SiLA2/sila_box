# SiLA–BOX

The SiLA–BOX manages the SiLA server/client and the network configurations, while running on a Raspberry Pi.
It allows a quick and easy integration of any desired
instrument into your network. All you need is at least a Raspberry Pi
Zero, which costs just a few bucks, and this operating system.
The simple web interface allows a quick configuration and monitoring,
so there is no need for special tools. To control your instrument you
can either download the appropriate SiLA server/client or if you are a techie
write it yourself. Then just upload the found server/client via the web
interface. Now you can control and monitor the instrument with SiLA.
All this costs you almost nothing in the end,
because this operating system is for free and if you want, you can even
participate into the development.

Here is a [slide deck](https://sila-standard.com/wp-content/uploads/2019/02/silaBOX_Manual.pdf) describing the SiLA Box in more detail.

# Requirements
 * Raspberry Pi Zero
 * Micro SD Card (at least 4GB, class 10 highly recommended)
 * Micro SD Card Reader

# Quick Start Installation

Currently there is no installation script available,
for a quick start without having to set up the operating system,
an image is available as download.

## Steps
 1. Download the [Image](http://zwergenbu.de:90/images/sila_node_latest.img.zip)
 1. Unzip
 1. Plugin the SD Card Reader with the SD Card in it
 1. Clone the Image to the SD Card
    * __Linux__: 
      * Find the device with ```sudo fdisk -l``` (here sdX)
      * ```sudo dd if=sila_node_latest.img of=/dev/sdX iflag=fullblock oflag=direct bs=4M; sudo sync```
    * __Windows__:
      * Download the Windows installer from [etcher.io](https://etcher.io/)
      * Run Etcher and select the unzipped image file
      * Select the SD card drive
      * Click Burn
      
# Operation
There are two different operating modes.

## 1. Configuration Mode

This mode becomes active when no device is connected to the Box.
So just ```plugin the sd card and the power connection```.
No matter which network configuration was previously set,
a default WLAN network is hosted by the Box.

As soon as the Box is booted,
you can ```connect to the WLAN``` network starting with ```silabox_*```.
The ```password is```, as always, ```silamaster```.

Now you can access the configuration page ([http://192.168.10.1](http://192.168.10.1)) via the browser.

```Note: SiLA drivers are also loaded in this mode.```

## 2. Connect Mode
If the Box has been configured as desired,
it is sufficient to connect any device via USB and the box connects to the configured network.



# Manual Installation
```
Note: The update function on the configuration page pulls from GitLab.
```
This short manual is intended for system developers.
After the installation script is available it will be clearer.
_This_ _is_ _not_ _a_ _pretty_ _solution!_


The current image is based on Raspbian, but theoretically any linux distribution can be used.

## Pre Boot

 * Enlarge the fist partition (logs and drivers are located here)
 * Boot the system normally

## As Root User
   
 * Create the user sila
 * Clone as user sila from this repo into sila`s home root
 * In sila`s home rename sila_box to sila_node
 * Recursively chown sila_node/WebUI to www-data
 * Recursively chmod sila_node/WebUI to 770 
 * Copy the ```*.conf, *.ini, interfaces, *.service``` to the corresponding paths in the system from the repo
 * Copy ```conf.yaml``` to ```/boot/SiLA/conf.yaml```
 * Install ```dnsmasq lighttpd python3-dev python-dev hostapd```
 * Reload systemctl ```systemctl daemon-reload```
 * Disable start at boot for: ```systemctl disable```
   * dhcpcd
   * wpa_supplicant
   * hostapd
   * dnsmasq
 * Enable start at boot for: ```systemctl enable```
   * lighttpd
   * pre_network
   * post_network
 * Edit crontab ```crontab -e```
   * Copy - Paste the contents of the *root_crontab* file.
 * Install ```jdk1.8.0_191``` to ```/opt/```
 
# Login
## SSH 
 * User: ```sila```
 * Password: ```silamaster```
## WLAN
 * Network: ```silabox_*```
 * Password: ```silamaster```
 
# Limitations 
 * Currently only WLAN Networks supported
 * Only one Node at a time
 * No installation script
 
 # Future
 * LAN support
 * Support for other Raspberry Pis models
 * Installation script
