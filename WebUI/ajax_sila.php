<?php

require_once __DIR__.'/vendor/autoload.php';
use Symfony\Component\Yaml\Yaml;

const NODE_FILES = "/boot/SiLA/*.jar";
const BOOT_HOME = "/boot/SiLA/";
const WEB_ROOT = "/home/sila/sila_node/WebUI/";

if ($_GET['action'] == 'loadSettings') {

    /***
     * Load configuration file
     */
    $config = Yaml::parseFile(BOOT_HOME . "conf.yaml");
    //var_dump($config);

    $nodes = array();
    try {
        foreach (glob(NODE_FILES) as $file){
            $nodes[] = substr($file, 11);
        }
        //var_dump($nodes);
    } catch (Exception $exc) {
        //var_dump($exc);
    }

	$response = array();
    ob_start();
    require 'ajax/sila_settings.html';
	$response['html_content'] = ob_get_clean();

	echo json_encode($response);
} elseif ($_GET['action'] == 'uploadFile'){
    // Check if file was uploaded without errors
    $response = array();
    $response["success"] = false;

    if(isset($_FILES["node"]) && $_FILES["node"]["error"] == 0){
        $allowed = array("jar" => "application/x-java-archive");
        $filename = $_FILES["node"]["name"];
        $filetype = $_FILES["node"]["type"];
        $filesize = $_FILES["node"]["size"];

        // Verify file extension
        $ext = pathinfo($filename, PATHINFO_EXTENSION);
        if(array_key_exists($ext, $allowed)){
            $success = move_uploaded_file($_FILES["node"]["tmp_name"], $_SERVER['DOCUMENT_ROOT'] . "/upload/" . $filename);
            if($success){
                $response["success"] = true;
                $response["msg"] = "Your file was uploaded successful.";
            } else{
                $response["msg"] = "There was a problem uploading your file. Please try again.";
            }
        } else {
            $response["msg"] = "Please select a valid file format.";
        }
    } else{
        $response["msg"] = $_FILES["node"]["error"];
    }

    header('Content-Type: application/json');

    $file = fopen(WEB_ROOT . "ctrl/upload", "w");
    fwrite($file, "ok");
    fclose($file);

    echo json_encode($response);

} elseif ($_GET['action'] == 'saveConfig') {
    //var_dump($_POST);
    $config =
                    array("wlan" =>
                        array("ssid" => $_POST['ssid'], "psk" => $_POST['psk']),
                    "sila" => array("node" => $_POST['node'], "param" => $_POST['param']));


    if (!empty($_POST['ap'])) {
         $config['ap'] = $_POST['ap'];
    }
    if (!empty($_POST['browser_port'])) {
         $config['browser'] = array("port" => $_POST['browser_port']);
    }

    //var_dump($config);

    $yaml = Yaml::dump($config);
    //var_dump($yaml);
    file_put_contents(WEB_ROOT . "ctrl/conf.yaml", $yaml);

    header('Content-Type: application/json');
    $response = array("success" => true, "msg" => "Configuration saved!");
    echo json_encode($response);

} elseif ($_GET['action'] == 'loadLog'){
    $response = array();
    ob_start();
    require 'ajax/sila_log.html';
    $response['html_content'] = ob_get_clean();

    echo json_encode($response);

} elseif ($_GET['action'] == 'loadDocu'){
    $response = array();
    ob_start();
    require 'ajax/sila_docu.html';
    $response['html_content'] = ob_get_clean();

    echo json_encode($response);

} elseif ($_GET['action'] == 'getLog'){
    $file = fopen("/boot/SiLA/" . $_GET['file'], "r");
    $read = fread($file,filesize("/boot/SiLA/" . $_GET['file']));
    fclose($file);

    echo $read;

} elseif ($_GET['action'] == 'restartNode'){
    $response =  array("success" => false);
    try {
        $file = fopen(WEB_ROOT . "ctrl/start", "w");
        fwrite($file, " ");
        fclose($file);
        $response["success"] = true;
        $response["msg"] = "Your node was restarted successfully!";
    } catch (Exception $exc) {
        $response["msg"] = "Something went wrong with restarting your node. Try again!";
    }

    header('Content-Type: application/json');
    echo json_encode($response);

} elseif ($_GET['action'] == 'stopNode'){
    $response =  array("success" => false);
    try {
        $file = fopen(WEB_ROOT . "ctrl/stop", "w");
        fwrite($file, " ");
        fclose($file);
        $response["success"] = true;
        $response["msg"] = "Your node was stopped successfully!";
    } catch (Exception $exc) {
        $response["msg"] = "Something went wrong with restarting your node. Try again!";
    }

    header('Content-Type: application/json');
    echo json_encode($response);

} elseif ($_GET['action'] == 'powerOff'){
    $response =  array("success" => false);
    try {
        $file = fopen(WEB_ROOT . "ctrl/poweroff", "w");
        fwrite($file, " ");
        fclose($file);
        $response["success"] = true;
        $response["msg"] = "The system will shutdown soon!";
    } catch (Exception $exc) {
        $response["msg"] = "Something went wrong with shutting down the system. Try again!";
    }

    header('Content-Type: application/json');
    echo json_encode($response);
} elseif ($_GET['action'] == 'updateSystem'){
    $response =  array("success" => false);
    try {
        $file = fopen(WEB_ROOT . "ctrl/update", "w");
        fwrite($file, " ");
        fclose($file);
        $response["success"] = true;
        $response["msg"] = "The system will update soon!";
    } catch (Exception $exc) {
        $response["msg"] = "Something went wrong with updating the system. Try again!";
    }

    header('Content-Type: application/json');
    echo json_encode($response);
} elseif ($_GET['action'] == 'isRunning'){

    $filename = "ctrl/running";
    if (file_exists($filename)) {
        $response["success"] = true;
        $fd = fopen($filename, "r") or die("Unable to open file!");
        $response["read"] = fread($fd,filesize($filename));
        fclose($fd);
    } else {
        $response["success"] = false;
    }
    echo json_encode($response);

}