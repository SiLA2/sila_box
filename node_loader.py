import yaml
import sys
import os
import subprocess
import signal
import shlex
import logging
import shutil
from threading import Lock

CONFIG_FILE = "/boot/SiLA/conf.yaml"
# like the win user at mounted sd card would see the path
CONFIG_PATH_RELATIVE = "Partition 0, SiLA/conf.yaml"
WORKING_DIR_RELATIVE = "Partition 0, SiLA/"
WORKING_DIR = "/boot/SiLA/"
SUP_FILES = ".jar"
WEB_HOME = "/home/sila/sila_node/WebUI/"

NODE_LOG = WORKING_DIR + "node.log"
BROWSER_LOG = WORKING_DIR + "browser.log"
SYSTEM_LOG = WORKING_DIR + "node_loader.log"
PID_FILE = "/run/node_loader.pid"
RUNNER_INFO = WEB_HOME + "ctrl/running"

ENV = os.environ.copy()
ENV["PATH"] = "/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin:/usr/local/games:/usr/games:" + ENV["PATH"]
ENV["JAVA_HOME"] = "/opt/jdk1.8.0_191"
#ENV['JAVA_OPTS'] = ""

logging.basicConfig(level=logging.DEBUG,
                    format='%(asctime)s %(name)-12s %(levelname)-8s %(message)s',
                    datefmt='%m-%d %H:%M',
                    filename=SYSTEM_LOG,
                    filemode='w')

logger = logging.getLogger("node_loader")

pid = str(os.getpid())
f = open(PID_FILE, 'w')
f.write(pid)
f.close()

if not os.path.isfile(CONFIG_FILE):
    logger.error("can not find %s" % CONFIG_PATH_RELATIVE)
    sys.exit(1)

cfg_data = {}
try:
    with open(CONFIG_FILE, 'r') as stream:
        cfg_data = yaml.load(stream)
except:
    logger.exception("is the %s missing? recheck syntax." % CONFIG_PATH_RELATIVE)
    sys.exit(1)

# browser handling
################################################################################################################
browser = False
browser_port = 8080
process_browser = None
process_browser_log = None
try:
    browser_conf = cfg_data.get("browser", {})
    if len(browser_conf) != 0:
        browser = True
        browser_port = int(browser_conf.get("port", 8080))
        logger.info("read from config. start browser port: %d" % browser_port)
    else:
        logger.info("read from config. do not start browser")

except:
    logger.exception("recheck syntax." % CONFIG_PATH_RELATIVE)
    sys.exit(1)
try:
    if browser:
        if not os.path.isfile(WEB_HOME + "sila_browser.jar"):
            logger.error("browser was enabled but %s not found." % (WEB_HOME + "sila_browser.jar"))
        else:
            args = shlex.split("java -jar %s -p %d" % (WEB_HOME + "sila_browser.jar", browser_port))
            process_browser = subprocess.Popen(args, stdout=subprocess.PIPE, stderr=subprocess.STDOUT, env=ENV)
            log_args = shlex.split("rotatelogs -n 5 %s 1M" % BROWSER_LOG)
            process_browser_log = subprocess.Popen(log_args, stdin=process_browser.stdout)
            f = open(RUNNER_INFO, 'w')
            f.write("Browser")
            f.close()
            shutil.chown(RUNNER_INFO, user="www-data", group="www-data")
except:
    logger.exception("can not start browser")

# node handling
################################################################################################################
node = []
param = []
process_java = None
process_log = None
try:
    sila_conf = cfg_data.get("sila", {})
    node = sila_conf.get("node", "")
    param = sila_conf.get("param", "")
    logger.info("read from config. node: %s, param: %s" % (node, param))
except:
    logger.exception("recheck syntax." % CONFIG_PATH_RELATIVE)
    sys.exit(1)

try:
    if node == "" or node == "no_node":
        logger.info("no node selected")
    elif not os.path.isfile(WORKING_DIR + node):
        logger.error("can not find %s node" % (WORKING_DIR_RELATIVE + node))
    elif not node.endswith(".jar"):
        logger.error("can not load %s node. supported %s" % (WORKING_DIR_RELATIVE + node, SUP_FILES))
    else:
        args = shlex.split("java -jar %s %s" % (WORKING_DIR + node, param))
        process_java = subprocess.Popen(args, stdout=subprocess.PIPE, stderr=subprocess.STDOUT, env=ENV)
        logger.info("running %s node with param %s" % (node, param))

        log_args = shlex.split("rotatelogs -n 5 %s 1M" % NODE_LOG)
        process_log = subprocess.Popen(log_args, stdin=process_java.stdout)
        f = open(RUNNER_INFO, 'w')
        f.write("Node")
        f.close()
        shutil.chown(RUNNER_INFO, user="www-data", group="www-data")
except:
    logger.exception("cant load %s node with param %s" % (node, param))

# kill handling
################################################################################################################
lock = Lock()
lock.acquire()


def _exit(signum, frame):
    global lock
    lock.release()


def _kill(process):
    if process is not None:
        process.kill()


# w8 for kill signal
signal.signal(signal.SIGINT, _exit)
signal.signal(signal.SIGTERM, _exit)
lock.acquire()

_kill(process_java)
_kill(process_log)

_kill(process_browser)
_kill(process_browser_log)

os.remove(WEB_HOME + "ctrl/running")
os.remove(PID_FILE)