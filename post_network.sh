#!/bin/bash

#PARAMS
DEFAULT_NUMBER_OF_USB_DEV=1
WI_DEVICE="wlan0"
CONFIG_FILE="/boot/SiLA/conf.yaml"

if [ $(lsusb | wc -l) -eq $DEFAULT_NUMBER_OF_USB_DEV ] || [[ $(cat $CONFIG_FILE) == *ap:* ]];then
    echo "set ip"
    ifconfig  $WI_DEVICE 192.168.10.1 netmask 255.255.255.0
    sleep 1
    echo "start hostapd"
    systemctl start hostapd
    sleep 1
    echo "start dnsmasq"
    systemctl start dnsmasq
else
    systemctl start dhcpcd
    sleep 30
    echo "nameserver 8.8.8.8" >> /etc/resolv.conf
fi
